import serial
import struct
from time import ctime,sleep

"""
Binary protocal decode (HEX)

header
ff 55
0  1

command body
len idx action device port slot data a
2   3   4      5      6    7    8


"""
def short2bytes(sval):
    val = struct.pack("h",sval)
    print(val)
    return [val[0],val[1]]


def stop_motors(ser):
    print("motor stopping")
    ser.write(bytearray([0xff,0x55,0x07,0x00,0x02,0x05,   0x00, 0x00, 0x00, 0x00]))
    print( ser.readline() )

with serial.Serial('/dev/tty.wchusbserial21110', 115200, timeout=1) as ser:
    ser.flushInput()
    # head = b'\xff\x55'
    # ser.write(head + b'\x06\x00\x02\x22\x09\x00\x00\x0a\r')       #These need to be bytes not unicode
    # # skt.send(b'\x12\r')
    # line = ser.readline()   # read a '\n' terminated line
    # print(line)
    # ser.write(head + b'\x04\x03\x01\x01\x01\x0a')
    # # ser.write(b"a")     # write a string
    # line = ser.readline()   # read a '\n' terminated line
    # print( ser.readline() )

    # foward motor 100, 100
    # print("motor run ")
    # print(short2bytes(100))
    # import IPython
    # IPython.embed()
    # wide circle using JoyStick (x05)
    # ser.write(bytearray([0xff,0x55,0x07,0x00,0x02, 0x05] + [0x0d, 0x00] + [0x0d, 0x0d] ))
    # ser.write(bytearray([0xff,0x55,0x07,0x00,0x02,0x05] + [0x9c, 0x00] + [0x00, 0x00] ))

    # command body
    # len idx action device port slot data a
    # 2   3   4      5      6    7    8

    # servo move xB
    print('run Servo')
    ser.write(bytearray([0xff,0x55,0x07,0x00,0x02, 0xB] + [0x01] + [0x01] + [0xB4]))
    print( ser.readline() )
    ser.write(bytearray([0xff,0x55,0x07,0x00,0x02, 0xB] + [0x01] + [0x01] + [0x00]))
    print( ser.readline() )

    sleep(5)
    # motor stop
    stop_motors(ser)

    ser.close()
    # import IPython
    # IPython.embed()
